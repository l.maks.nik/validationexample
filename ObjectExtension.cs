using System;

namespace Validation {
	public static class ObjectExtension {
		
		public static bool IsNumericType(this object o) {   
			switch (Type.GetTypeCode(o.GetType())) {
				case TypeCode.Byte:
				case TypeCode.SByte:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Single:
					return true;
				default:
					return false;
			}
		}

		public static bool IsDateTimeType(this object o) {
			switch (Type.GetTypeCode(o.GetType())) {
				case TypeCode.DateTime:
					return true;
				default:
					return false;
			}
		}

		public static int Compare(this object left, object right) {
			switch (Type.GetTypeCode(left.GetType())) {
				case TypeCode.Byte:
					return ((Byte)left).CompareTo(right);
				case TypeCode.SByte:
					return ((SByte)left).CompareTo(right);
				case TypeCode.UInt16:
					return ((UInt16)left).CompareTo(right);
				case TypeCode.UInt32:
					return ((UInt32)left).CompareTo(right);
				case TypeCode.UInt64:
					return ((UInt64)left).CompareTo(right);
				case TypeCode.Int16:
					return ((Int16)left).CompareTo(right);
				case TypeCode.Int32:
					return ((Int32)left).CompareTo(right);
				case TypeCode.Int64:
					return ((Int64)left).CompareTo(right);
				case TypeCode.Decimal:
					return ((Decimal)left).CompareTo(right);
				case TypeCode.Double:
					return ((Double)left).CompareTo(right);
				case TypeCode.Single:
					return ((Single)left).CompareTo(right);
				case TypeCode.DateTime:
					return ((DateTime)left).CompareTo(right);
				default:
					throw new Exception($"Type \"{left.GetType().FullName}\" is not supported for compare");
			}
		}

	}
}