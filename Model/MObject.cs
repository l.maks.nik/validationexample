﻿using System;
using System.ComponentModel.DataAnnotations;
using Validation.Attributes;

namespace Validation.Model {
	public class MObject {

		[Range(1, 5)]
		[LessOrEqualThan(nameof(Max))]
		public int Min { get; set; }
		
		[Range(5, 10)]
		public int Max { get; set; }

		
		public DateTime Begin { get; set; }
		
		[MoreThan(nameof(Begin))]
		public DateTime End { get; set; }

		[Required]
		public string Name { get; set; }

		[RequiredOr(nameof(Obj2), nameof(Obj3))]
		public object Obj1 { get; set; }
		public object Obj2 { get; set; }
		public object Obj3 { get; set; }
	}
}
