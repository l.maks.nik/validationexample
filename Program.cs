﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Validation.Model;

namespace Validation {
	class Program {
		static void Main(string[] args) {
			var date = DateTime.Now;

			var obj = new MObject() {
				Min = 5,
				Max = 5,
				Begin = date.AddDays(-1),
				End = date,
				Name = "dddd",
				Obj1 = null,
				Obj2 = null,
				Obj3 = new object()
			};


			var results = new List<ValidationResult>();
			Validator.TryValidateObject(obj, new ValidationContext(obj, null, null), results, true);

			if (results.Any()) {
				foreach (var result in results) {
					Console.WriteLine($"{result.MemberNames.Aggregate("", (s, s1) => s += $"{s1},").TrimEnd(',')}: {result.ErrorMessage}");
				}
			}
			else {
				Console.WriteLine("All OK!");
			}


			Console.ReadKey();

		}
	}
}
