using System;
using System.ComponentModel.DataAnnotations;

namespace Validation.Attributes {
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class MoreOrEqualThanAttribute : ValidationAttribute {

		public MoreOrEqualThanAttribute(string propertyName) {
			_thenPropertyName = propertyName;
		}

		private readonly string _thenPropertyName;

		protected override ValidationResult IsValid(object value, ValidationContext context) {
			var result = base.IsValid(value, context);

			if (result != ValidationResult.Success || value == null) {
				return ValidationResult.Success;
			}

			if (!value.IsNumericType() && !value.IsDateTimeType()) {
				return new ValidationResult("Value is not Numeric or DateTime types.", new []{context.MemberName});
			}
			
			var property = context.ObjectType.GetProperty(_thenPropertyName);
			var field = context.ObjectType.GetField(_thenPropertyName);
			
			if (property == null && field == null) 
				return new ValidationResult("Unknown property or field", new[] {_thenPropertyName});

			var thenValue = property?.GetValue(context.ObjectInstance) ?? field.GetValue(context.ObjectInstance) ?? new object();
			if (!thenValue.IsNumericType() && !thenValue.IsDateTimeType()) {
				return new ValidationResult("Value to compare is not Numeric or DateTime types.", new []{_thenPropertyName});
			}
			
			
			if (value.Compare(thenValue) < 0) {
				string[] memberNames = context.MemberName != null
					? new[] { context.MemberName }
					: null;
				if (string.IsNullOrWhiteSpace(ErrorMessage))
					ErrorMessage =  $"The value of field {context.MemberName} cannot be less then the value of field {_thenPropertyName}";
				
				result = new ValidationResult(ErrorMessage, memberNames);
			}

			return result;
		}

		public override bool IsValid(object value) {
			return true;
		}

	}
}