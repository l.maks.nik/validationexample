using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Validation.Attributes {
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class RequiredOrAttribute : RequiredAttribute {

		private readonly string[] _orPropertyNames;
	
		public RequiredOrAttribute(params string[] propertyNames) {
			_orPropertyNames = propertyNames;
		}

		protected override ValidationResult IsValid(object value, ValidationContext context) {
			if (IsValid(value)) return ValidationResult.Success;

			bool isValid = false;
			foreach (var name in _orPropertyNames) {
				var property = context.ObjectType.GetProperty(name);
				var field = context.ObjectType.GetField(name);
			
				if (property == null && field == null) 
					return new ValidationResult("Unknown property or field", new[] {name});

				isValid = IsValid(property?.GetValue(context.ObjectInstance) ?? field?.GetValue(context.ObjectInstance));
				if (isValid) break;
			}
			
			if (isValid) return ValidationResult.Success;
			
			return new ValidationResult($"One of the fields {context.MemberName}, {_orPropertyNames.Aggregate("", (s, s1) => s += $"{s1},").TrimEnd(',')} is required", 
				new []{context.MemberName});
		}

	}
}