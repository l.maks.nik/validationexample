using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Validation.Attributes {
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
	public class ValidateObjectAttribute : ValidationAttribute {
		protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
			var results = new List<ValidationResult>();

			if (value is IEnumerable) {
				foreach (var obj in (IEnumerable)value) {
					if (obj == null) continue;
					Validator.TryValidateObject(obj, new ValidationContext(obj, null, null), results, true);
				}
			} else
				Validator.TryValidateObject(value, new ValidationContext(value, null, null), results, true);

			if (results.Count != 0) {
				if (results.Count == 1) {
					var res = results.First();
					return new ValidationResult(res.ErrorMessage, res.MemberNames.Select(name => $"{validationContext.MemberName}.{name}").ToArray());
				}

				var msg = "";
				foreach (var result in results) {
					msg += $"{result.MemberNames.Aggregate("", (s, s1) => s += $"{s1},").TrimEnd(',')} - {result.ErrorMessage}";
				}
				return new ValidationResult($"The field has some errors: \"{msg}\"", new []{validationContext.MemberName});
			}

			return ValidationResult.Success;
		}
	}
}